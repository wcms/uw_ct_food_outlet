<?php

/**
 * @file
 * uw_ct_food_outlet.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_food_outlet_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_food_outlet content'.
  $permissions['create uw_food_outlet content'] = array(
    'name' => 'create uw_food_outlet content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_food_outlet content'.
  $permissions['delete any uw_food_outlet content'] = array(
    'name' => 'delete any uw_food_outlet content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_food_outlet content'.
  $permissions['delete own uw_food_outlet content'] = array(
    'name' => 'delete own uw_food_outlet content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in cuisines'.
  $permissions['delete terms in cuisines'] = array(
    'name' => 'delete terms in cuisines',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uw_food_outlet_types'.
  $permissions['delete terms in uw_food_outlet_types'] = array(
    'name' => 'delete terms in uw_food_outlet_types',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in uw_payment_type'.
  $permissions['delete terms in uw_payment_type'] = array(
    'name' => 'delete terms in uw_payment_type',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any uw_food_outlet content'.
  $permissions['edit any uw_food_outlet content'] = array(
    'name' => 'edit any uw_food_outlet content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_food_outlet content'.
  $permissions['edit own uw_food_outlet content'] = array(
    'name' => 'edit own uw_food_outlet content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in cuisines'.
  $permissions['edit terms in cuisines'] = array(
    'name' => 'edit terms in cuisines',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uw_food_outlet_types'.
  $permissions['edit terms in uw_food_outlet_types'] = array(
    'name' => 'edit terms in uw_food_outlet_types',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uw_payment_type'.
  $permissions['edit terms in uw_payment_type'] = array(
    'name' => 'edit terms in uw_payment_type',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter uw_food_outlet revision log entry'.
  $permissions['enter uw_food_outlet revision log entry'] = array(
    'name' => 'enter uw_food_outlet revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'merge cuisines terms'.
  $permissions['merge cuisines terms'] = array(
    'name' => 'merge cuisines terms',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'term_merge',
  );

  // Exported permission: 'merge uw_food_outlet_types terms'.
  $permissions['merge uw_food_outlet_types terms'] = array(
    'name' => 'merge uw_food_outlet_types terms',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'term_merge',
  );

  // Exported permission: 'merge uw_payment_type terms'.
  $permissions['merge uw_payment_type terms'] = array(
    'name' => 'merge uw_payment_type terms',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'term_merge',
  );

  // Exported permission: 'override uw_food_outlet authored by option'.
  $permissions['override uw_food_outlet authored by option'] = array(
    'name' => 'override uw_food_outlet authored by option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_food_outlet authored on option'.
  $permissions['override uw_food_outlet authored on option'] = array(
    'name' => 'override uw_food_outlet authored on option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_food_outlet comment setting option'.
  $permissions['override uw_food_outlet comment setting option'] = array(
    'name' => 'override uw_food_outlet comment setting option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_food_outlet promote to front page option'.
  $permissions['override uw_food_outlet promote to front page option'] = array(
    'name' => 'override uw_food_outlet promote to front page option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_food_outlet published option'.
  $permissions['override uw_food_outlet published option'] = array(
    'name' => 'override uw_food_outlet published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_food_outlet revision option'.
  $permissions['override uw_food_outlet revision option'] = array(
    'name' => 'override uw_food_outlet revision option',
    'roles' => array(),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_food_outlet sticky option'.
  $permissions['override uw_food_outlet sticky option'] = array(
    'name' => 'override uw_food_outlet sticky option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  return $permissions;
}
