<?php

/**
 * @file
 * uw_ct_food_outlet.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_ct_food_outlet_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Cafe',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'uuid' => '0a4a713f-580c-4932-8f05-155fabc8e334',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_food_outlet_types',
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
  );
  $terms[] = array(
    'name' => 'Restaurant',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 4,
    'uuid' => '628eaad4-eba1-4289-9bd8-117a83284a27',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_food_outlet_types',
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
  );
  $terms[] = array(
    'name' => 'Residence',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 3,
    'uuid' => '69fb1b79-c14b-4e76-b99c-bf1a3cd75233',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_food_outlet_types',
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
  );
  $terms[] = array(
    'name' => 'Administration',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 5,
    'uuid' => '6acfc021-6cf0-47f1-b0b9-5b87e644580d',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_food_outlet_types',
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
  );
  $terms[] = array(
    'name' => 'Franchise',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 2,
    'uuid' => '77417d7f-4d95-4437-a723-6ff12ca88414',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_food_outlet_types',
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
  );
  $terms[] = array(
    'name' => 'Eatery',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 1,
    'uuid' => '784872d9-a962-4136-a04f-03e913a27d63',
    'language' => 'und',
    'i18n_tsid' => 0,
    'vocabulary_machine_name' => 'uw_food_outlet_types',
    'metatags' => array(
      'en' => array(
        'robots' => array(
          'value' => array(
            'index' => 0,
            'follow' => 0,
            'noindex' => 0,
            'nofollow' => 0,
            'noarchive' => 0,
            'nosnippet' => 0,
            'noodp' => 0,
            'noydir' => 0,
            'noimageindex' => 0,
            'notranslate' => 0,
          ),
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
    'translations' => array(
      'original' => NULL,
      'data' => array(),
    ),
  );
  return $terms;
}
