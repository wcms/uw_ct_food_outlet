<?php

/**
 * @file
 * uw_ct_food_outlet.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_food_outlet_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = TRUE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'food_outlet_block';
  $context->description = 'Show food outlets on one page';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'locations-and-hours' => 'locations-and-hours',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Show food outlets on one page');
  $export['food_outlet_block'] = $context;

  return $export;
}
