/**
 * @file
 * Adds JavaScript for "friendlier" behaviours when JS is available.
 */
(function ($) {
  "use strict";
  Drupal.behaviors.foodOutlet = {

    attach: function (context, settings) {

      $('.franchise-tabs button').each(function () {

        if ($(this).attr('aria-selected') == 'false') {

          $('#franchise-menus button[aria-selected="true"]').attr('tabindex', '-1');

          $('#franchise-menus button[aria-selected="true"]').attr('aria-selected', 'false');

          $(this).attr('aria-selected', 'true');

          $(this).removeAttr('tabindex');

          $('.fm-tabcontent[aria-hidden="false"]').attr('aria-hidden', 'true');

          $('#franchise-menus #' + $(this).attr('aria-controls')).attr('aria-hidden', 'false');

        }
      });
    }
  };
}(jQuery));
