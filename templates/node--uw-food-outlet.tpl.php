<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct url of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type, i.e., "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type, i.e. story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined, e.g. $node->body becomes $body. When needing to access
 * a field's raw values, developers/themers are strongly encouraged to use these
 * variables. Otherwise they will have to explicitly specify the desired field
 * language, e.g. $node->body['en'], thus overriding any language negotiation
 * rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */
?>

<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="node-inner">

    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>

    <div class="content_node"<?php print $content_attributes; ?>>
      <?php
      // We hide the comments and links now so that we can render them later.
      // We hide the date closed, hours change and hours notice so we can add
      // them back with additional styling to match the food outlet block view.
      hide($content);
      ?>

      <div class="center">
        <?php
        print render($content['field_outlet_location'][0]['#markup']);
        hide($content['field_outlet_location']);
        ?>
      </div>

      <hr>

      <?php
      // Get the names of all the accepted payment types, and output them comma-separated.
      // Not sure this is the best way to do this, but the function that was here before didn't work properly when there was only one payment type.
      // It also wasn't commented, and we couldn't figure out the intent.
      // This method checks for duplicates and doesn't display them. Not sure what the original would have done.
      if (!empty($content['field_payment_accepted']['#object']->field_payment_accepted[LANGUAGE_NONE])) {
        $accepted_payment = [];
        foreach ($content['field_payment_accepted']['#object']->field_payment_accepted[LANGUAGE_NONE] as $payment_type) {
          $payment_name = check_plain(trim($payment_type['taxonomy_term']->name));
          if (!in_array($payment_name, $accepted_payment)) {
            $accepted_payment[] = $payment_name;
          }
        }
        if (!empty($accepted_payment)) {
          print '<div class="payment">' . implode(', ', $accepted_payment) . '</div>';
        }
      }
      ?>

      <div class="outlet-info container">

        <?php if (!empty($content['field_description'])): ?>
          <div class="outlet-description grid-description">
            <?php print '<h2>More Information</h2>'; ?>
            <?php print render($content['field_description']); ?>
          </div>
        <?php endif; ?>

        <?php if (!empty($content['field_contact'])): ?>
          <div class="food-contact grid-contact">
            <?php print '<h2>Contact</h2>'; ?>
            <?php print render($content['field_contact'][0]['entity']['paragraphs_item']); ?>
          </div>
        <?php endif; ?>

        <div class="location-hours grid-hours">

          <?php if (!empty($content['field_opening_hours'][0])): ?>
            <?php
              print '<h2>Hours</h2>';
              print render($content['field_opening_hours']);
            ?>
          <?php endif; ?>
          <?php $exceptions_list = []; ?>
          <?php if ((isset($node->field_date_closed) && !empty($node->field_date_closed[LANGUAGE_NONE][0]))
                 || (isset($node->field_hours_change) && !empty($node->field_hours_change[LANGUAGE_NONE][0]))) {
            // Exception - Closed (field_date_closed) display.
            if (isset($node->field_date_closed[LANGUAGE_NONE])) {
              $closed_fields = $node->field_date_closed[LANGUAGE_NONE];
            }
            if (isset($closed_fields)) {
              foreach ($closed_fields as $item) {
                $value = $item['value'];
                $value2 = $item['value2'];

                // Remove time part, only has yyyy-mm-dd.
                $close_date_from = substr($value, 0, -9);
                $close_date_to = substr($value2, 0, -9);

                // Create date from string 'yyyy-mm-dd'.
                $date = date_create($close_date_from);
                $date2 = date_create($close_date_to);

                // Exceptions - field_date_closed: no End Date.
                if (strcmp($close_date_from, $close_date_to) === 0) {
                  $display_date = date_format($date, "M. j");
                  $exceptions_list[] = '<li> ' . $display_date . ' <span class="oh-display-closed">Closed</span></li>';
                }
                else {
                  $close_year_from = substr($close_date_from, 0, 4);
                  $close_year_to = substr($close_date_to, 0, 4);
                  $close_month_from = substr($close_date_from, 5, 2);
                  $close_month_to = substr($close_date_to, 5, 2);

                  $display_month_from_format = date_format($date, "M");
                  $display_month_to_format = date_format($date2, "M");
                  $display_date_from_format = date_format($date, "j");
                  $display_date_to_format = date_format($date2, "j");

                  // Exception - ield_date_closed: has End Date (same year).
                  if (strcmp($close_year_from, $close_year_to) === 0) {
                    // Exception - Closed: has End Date (same year, same month and different date.)
                    if (strcmp($close_month_from, $close_month_to) === 0) {
                      $exceptions_list[] = '<li> ' . $display_month_from_format . '. ' . $display_date_from_format . ' - ' . $display_date_to_format . ' <span class="oh-display-closed">Closed</span></li>';
                    } // Exception - ield_date_closed: has End Date (same year, different month and different date)
                    else {
                      $exceptions_list[] = '<li> ' . $display_month_from_format . '. ' . $display_date_from_format . ' - ' . $display_month_to_format . '. ' .
                        $display_date_to_format . ' <span class="oh-display-closed">Closed</span></li>';
                    }
                  } // Exception - ield_date_closed: has End Date (different year).
                  else {
                    $exceptions_list[] = '<li> ' . $display_month_from_format . '. ' . $display_date_from_format . ', ' . $close_year_from . ' - ' .
                      $display_month_to_format . '. ' . $display_date_to_format . ', ' . $close_year_to . ' <span class="oh-display-closed">Closed</span></li>';
                  }
                }
              }
            }
          }

          // Exception - Hours Change (field_hours_change) display.
          if (isset($node->field_hours_change[LANGUAGE_NONE])) {
            $change_fields = $node->field_hours_change[LANGUAGE_NONE];
          }
          if (isset($change_fields)) {
            foreach ($change_fields as $item) {
              // Get date from and date to values.
              $change_value = $item['value'];
              $change_value2 = $item['value2'];

              // Remove time part, only has yyyy-mm-dd.
              $change_date_from = substr($change_value, 0, -9);
              $change_date_to = substr($change_value2, 0, -9);

              // Remove yyyy-mm-dd part, only has time part.
              $change_time_from = substr($change_value, 11, 5);
              $change_time_to = substr($change_value2, 11, 5);

              // Change time from 24-hour time to 12-hour time.
              $time_from_12_hour = date("g:i a", strtotime($change_time_from));
              $time_to_12_hour = date("g:i a", strtotime($change_time_to));

              // Create date from string 'yyyy-mm-dd'.
              $change_date = date_create($change_date_from);
              $change_date2 = date_create($change_date_to);

              // Exceptions - field_hours_change: no End Date.
              if (strcmp($change_date_from, $change_date_to) === 0 && strcmp($change_time_from, $change_time_to) === 0) {
                $display_change_date = date_format($change_date, "M. j");
                $exceptions_list[] = '<li> ' . $display_change_date . ' ' . $time_from_12_hour . '</li>';
              }
              else {
                $change_year_from = substr($change_date_from, 0, 4);
                $change_year_to = substr($change_date_to, 0, 4);
                $change_month_from = substr($change_date_from, 5, 2);
                $change_month_to = substr($change_date_to, 5, 2);

                $display_change_month_from_format = date_format($change_date, "M");
                $display_change_month_to_format = date_format($change_date2, "M");
                $display_change_date_from_format = date_format($change_date, "j");
                $display_change_date_to_format = date_format($change_date2, "j");

                // Exceptions - field_hours_change: has End Date (same year).
                if (strcmp($change_year_from, $change_year_to) === 0) {

                  // Exception - field_hours_change: has End Date (same year, same month.)
                  if (strcmp($change_month_from, $change_month_to) === 0) {
                    // Exception - field_hours_change: has End Date (same year, same month, same day.)
                    if (strcmp($change_date_from, $change_date_to) === 0) {
                      $exceptions_list[] = '<li> ' . $display_change_month_from_format . '. ' . $display_change_date_from_format . ' <span class="oh-display-times">' . $time_from_12_hour . ' - ' . $time_to_12_hour . '</span></li>';
                    } // Exception - field_hours_change: has End Date (same year, same month, different day.)
                    else {
                      $exceptions_list[] = '<li> ' . $display_change_month_from_format . '. ' . $display_change_date_from_format . ' ' . $time_from_12_hour . ' - ' .
                        $display_change_month_to_format . '. ' . $display_change_date_to_format . ' ' . $time_to_12_hour . ' <span class="oh-display-times"></span></li>';
                    }
                  } // Exception - field_hours_change: has End Date (same year, different month and different day)
                  else {
                    $exceptions_list[] = '<li> ' . $display_change_month_from_format . '. ' . $display_change_date_from_format . ' ' . $time_from_12_hour . ' - ' . $display_change_month_to_format . '. ' .
                      $display_change_date_to_format . ' ' . $time_to_12_hour . ' <span class="oh-display-times"></span></li>';
                  }
                } // Exceptions - field_hours_change: has End Date (different year).
                else {
                  $exceptions_list[] = '<li> ' . $display_change_month_from_format . '. ' . $display_change_date_from_format . ', ' . $change_year_from . ' ' . $time_from_12_hour . ' - '
                    . $display_change_month_to_format . '. ' . $display_change_date_to_format . ', ' . $change_year_to . ' ' . $time_to_12_hour . '</li>';
                }
              }
            }
          }
          if ($exceptions_list) {
            print '<ul class="exceptions_list"><span><strong>Exceptions:</strong></span>' . implode("\n", $exceptions_list) . '</ul>';
          }
          if (!empty($content['field_hours_notice'][0])) {
            print render($content['field_hours_notice']);
          }
          ?>
        </div>
      </div>

    </div> <!-- /content-node -->
  </div> <!-- /node-inner -->
</div> <!-- /node-->

<?php if (isset($content['tabs'])): ?>
  <h2 class="franchise-menus-title"><?php print 'Menus'; ?></h2>
  <div id="franchises">

    <div class="franchise-tabs" role="tablist" aria-label="Franchises">

      <?php foreach ($content['tabs'] as $key => $tab): ?>

        <?php if (is_int($key)): ?>

          <button
            role="tab"
            aria-selected="<?php $key == 0 ? print 'true' : print 'false'; ?>"
            aria-controls="<?php print $tab['html_name']; ?>-tab"
            id="fm_id_<?php print ($tab['nid']); ?>" <?php
            if ($key !== 0) {
              print 'tabindex="-1"';
            } ?>
            data-fm-id="<?php print ($tab['nid']); ?>"
          >
            <?php print $tab['title']; ?>
          </button>

        <?php endif; ?>

      <?php endforeach; ?>

    </div>

    <div class="franchise-menus">
      <?php foreach ($content['tabs'] as $key => $tab): ?>
        <div
          role="tabpanel"
          class="franchise-menu"
          id="<?php print $tab['html_name']; ?>-tab"
          <?php $key == 0 ? print 'aria-hidden=false' : print 'aria-hidden=true' ?>
        >
          <?php print views_embed_view('franchise_block', 'franchise_block', $tab['nid']); ?>
        </div>
      <?php endforeach; ?>
    </div>

  </div>

<?php endif; ?>

<?php print render($content['comments']); ?>
