<?php

/**
 * @file
 * uw_ct_food_outlet.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function uw_ct_food_outlet_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_basic_information|node|uw_food_outlet|form';
  $field_group->group_name = 'group_basic_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_food_outlet';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Basic Information',
    'weight' => '1',
    'children' => array(
      0 => 'field_description',
      1 => 'field_image',
      2 => 'field_serves_cuisine',
      3 => 'field_features',
      4 => 'field_payment_accepted',
      5 => 'field_outlet_photo',
      6 => 'field_outlet_summary',
      7 => 'field_outlet_type',
      8 => 'field_uw_fo_franchise_menus',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_basic_information|node|uw_food_outlet|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_food_exceptions|node|uw_food_outlet|form';
  $field_group->group_name = 'group_food_exceptions';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_food_outlet';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_information';
  $field_group->data = array(
    'label' => 'Exceptions',
    'weight' => '15',
    'children' => array(
      0 => 'field_date_closed',
      1 => 'field_hours_change',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-food-exceptions field-group-fieldset ',
        'required_fields' => 1,
      ),
    ),
  );
  $field_groups['group_food_exceptions|node|uw_food_outlet|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_information|node|uw_food_outlet|form';
  $field_group->group_name = 'group_information';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_food_outlet';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Hours information',
    'weight' => '6',
    'children' => array(
      0 => 'field_hours_notice',
      1 => 'field_opening_hours',
      2 => 'group_food_exceptions',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Hours information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_information|node|uw_food_outlet|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_uw_location|node|uw_food_outlet|form';
  $field_group->group_name = 'group_uw_location';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'uw_food_outlet';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Location',
    'weight' => '3',
    'children' => array(
      0 => 'field_outlet_location',
      1 => 'field_uw_fs_location_link',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-uw-location field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_uw_location|node|uw_food_outlet|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Basic Information');
  t('Exceptions');
  t('Hours information');
  t('Location');

  return $field_groups;
}
